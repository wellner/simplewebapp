# SimpleApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.


## Env setup

We assume amplify is already installed on the machine

run the following comands

amplify init

npm install --save aws-amplify @aws-amplify/ui-angular

npm start

amplify add api

You can get API endpoint infromation by running:
amplify status

## Run local

For running the app localy:

npm start

Open the app in 2 browser windows (both at http://localhost:4200/) so that you have your app running side by side. When creating a new item in one window, you should see it come through in the other window in real-time.

## e2e tests

We will use Cypress to run end-to-end tests of a graphical user interface

1. install

npm add cypress --dev


2. open Cypress from your project root

./node_modules/.bin/cypress open

OR

npx cypress open