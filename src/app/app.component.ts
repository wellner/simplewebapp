import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { APIService } from './API.service';
import { Restaurant } from '../types/restaurant';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  title = 'simpleApp';
  public simpleAppForm: FormGroup;
  public messageAfterSubmit: boolean;

   /* declare restaurants variable */
   restaurants: Array<Restaurant>;

  constructor(private api: APIService, private fb: FormBuilder) {  }

  async ngOnInit() {
    this.simpleAppForm = this.fb.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      city: ['', Validators.required]
    });

    this.messageAfterSubmit = false;

    /* fetch restaurants when app loads */
    this.api.ListRestaurants().then(event => {
      this.restaurants = event.items;
    });

    /* subscribe to new restaurants being created */
    this.api.OnCreateRestaurantListener.subscribe( (event: any) => {
      const newRestaurant = event.value.data.onCreateRestaurant;
      this.restaurants = [newRestaurant, ...this.restaurants];
    });
  }

  public onCreate(restaurant: Restaurant) {
    if (restaurant.city.length < 2 ||
        restaurant.description.length < 4 ||
        restaurant.name.length < 4){
      this.messageAfterSubmit = true;
      console.log('all items must have values');
      alert('Data was not loaded. make sure all fields have data');
      return;
    }

    if (!restaurant.name.startsWith('amit', 0) ){
      alert('Data value does not start with the correct prefix ;)');
      return;
    }
    this.api.CreateRestaurant(restaurant).then(event => {
      console.log('item created!');
      this.simpleAppForm.reset();
      this.messageAfterSubmit = false;
      alert('Data was uploaded succesfuly!');
    })
    .catch(e => {
      console.log('error creating restaurant...', e);
    });
  }
}
